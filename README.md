# Introduction

SimpleBank was built from the ground-up it returns a JSON that makes it easy for developers and sysadmins to consume easily.

These docs describe how to use the SimpleBanking API. We hope you enjoy these docs.



## Use Cases

There are many reasons to use the SimpleBanking API. The most common use cases are to register as a customer,login,deposit into your account or someone else's account, withdraw from your account,request for account statement and generate account information.


## Authorization
The login,account statement,account information and withdraw API  requests require the use of user password and account number for authentication.

The login Api returns a jwt Token.

The registration api returns account information of the newly created user.

```http
Post /simpleBank/create_account
Post /simpleBank/login
Post /simpleBank/deposit
Post /simpleBank/withdrawal
Get /simpleBank/account_statement/{accountNumber}
Get /simpleBank/account_info/{accountNumber}
```

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `{accountNumber}` | `string` | **Required**. Your simpleBanking accountNumber |


## Responses

Many API endpoints return the JSON representation of the resources created or edited. However, if an invalid request is submitted, or some other error occurs, SimpleBanking returns a JSON response in the following format:
```javascript
{
  "message" : string,
  "success" : bool,
  "data"    : string
}
```

The `message` attribute contains a message commonly used to indicate errors or, in the case of deleting a resource, success that the resource was properly deleted.

The `success` attribute describes if the transaction was successful or not by returning either true or false.

The `data` attribute contains any other metadata associated with the response. This will be an escaped string containing JSON data.

## Status Codes

SimpleBanking returns the following status codes in its API:

| Status Code | Description |
| :--- | :--- |
| 200 | `OK` |
| 400 | `BAD REQUEST` |
