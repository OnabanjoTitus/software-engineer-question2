package com.SimpleBanking.application.data.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account implements UserDetails {
    private String accountNumber;
    private Double accountBalance;
    private String accountName;
    private String accountPassword;
    private Role role;
    private Collection<? extends GrantedAuthority> authorities;
    List<AccountStatements> accountStatementsList= new ArrayList<>();

    public static Account create(Account account) {
        log.info("Authority " + account.getRole().toString());

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(account.getRole().toString()));
        return new Account(
                account.getAccountNumber(),
                account.getAccountBalance(),
                account.getAccountName(),
                account.getPassword(),
                account.getRole(),
                authorities,
                account.getAccountStatementsList()
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority simpleGrantedAuthority= new SimpleGrantedAuthority(role.name());
        return Collections.singletonList(simpleGrantedAuthority);
    }

    @Override
    public String getPassword() {
        return accountPassword;
    }

    @Override
    public String getUsername() {
        return accountName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
