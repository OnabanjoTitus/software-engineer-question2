package com.SimpleBanking.application.data.model;

import lombok.Data;

@Data
public class AccountDto {
  private String accountName;
  private String accountPassword;
  private Double initialDeposit;
  private String accountNumber;

}
