package com.SimpleBanking.application.data.model;

import lombok.Data;

@Data
public class AccountObject {
    private String accountName;
    private String accountNumber;
    private Double accountBalance;

}
