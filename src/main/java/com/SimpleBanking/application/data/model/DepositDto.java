package com.SimpleBanking.application.data.model;

import lombok.Data;

@Data
public class DepositDto {
    private String accountNumber;
    private Double amount;
}
