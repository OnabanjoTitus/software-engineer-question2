package com.SimpleBanking.application.data.model;

import lombok.Data;

@Data
public class LoginDto {
    private String accountNumber;
    private String password;

}
