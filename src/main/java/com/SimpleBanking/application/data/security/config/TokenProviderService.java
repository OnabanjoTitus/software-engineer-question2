package com.SimpleBanking.application.data.security.config;

import com.SimpleBanking.application.data.model.Account;
import org.springframework.security.core.Authentication;

public interface TokenProviderService {
    String generateLoginToken(Authentication authentication, Account account);
}
