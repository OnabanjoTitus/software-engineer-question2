package com.SimpleBanking.application.data.security.config;

import com.SimpleBanking.application.data.model.Account;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Base64;
import java.util.Date;
import java.util.stream.Collectors;

@Slf4j
@Data
@Service
@Configuration
public class TokenProviderServiceImpl implements Serializable, TokenProviderService {


    @Override
    public String generateLoginToken(Authentication authentication, Account account) {
        final String authorities = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));

        log.info(authorities);

        String jwts = Jwts.builder()
                .setSubject(authentication.getName())
                .claim("roles: ", authorities)
                .setIssuer("Onabanjo Titus-DigiCore")

                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis()
                        + 864_000_00))
                .compact();
        log.info("JWT token with claims -> {}", jwts);
        return jwts;
    }


}
