package com.SimpleBanking.application.data.security.config;

import com.SimpleBanking.application.data.model.Account;
import com.SimpleBanking.application.data.model.AccountDto;
import com.SimpleBanking.application.data.model.LoginDto;
import com.SimpleBanking.application.services.BankServices;
import com.SimpleBanking.application.web.Exceptions.AccountException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class UserPrincipalService implements UserDetailsService {
    @Autowired
    BankServices bankServices;

    @Autowired
    private AppAuthenticationProvider authenticationManager;

    @Autowired
    TokenProviderServiceImpl tokenProviderService;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        log.info("Check this account number " + s);
       Account account = bankServices.findUserByAccountNumber(s);
        if(account==null){
            throw new UsernameNotFoundException("User with given account number not found");
        }
        else{

            return account;
        }
    }
    public JWTToken loginUser(LoginDto accountDto) throws UsernameNotFoundException, AccountException {
        log.info("got here on log-in****-->{}",accountDto);
        loadUserByUsername(accountDto.getAccountNumber());
        Account account=bankServices.findUserByAccountNumber(accountDto.getAccountNumber());
        if(!accountDto.getPassword().equals(account.getAccountPassword())){
            throw new AccountException("The password entered is incorrect");
        }
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        account.getAccountNumber(), account.getPassword()
                )
        );
        log.info("after authentication");
        log.info("security context authentication");
        SecurityContextHolder.getContext().setAuthentication(authentication);


        JWTToken jwtToken = new JWTToken(tokenProviderService.generateLoginToken(authentication, account));

        log.info("JWT object -> {}", jwtToken.toString());
        return jwtToken;


    }
}
