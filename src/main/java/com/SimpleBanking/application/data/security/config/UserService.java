package com.SimpleBanking.application.data.security.config;

import com.SimpleBanking.application.data.model.LoginDto;
import com.SimpleBanking.application.web.Exceptions.AccountException;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    JWTToken login(LoginDto accountDto) throws AccountException;
}
