package com.SimpleBanking.application.data.security.config;

import com.SimpleBanking.application.data.model.LoginDto;
import com.SimpleBanking.application.web.Exceptions.AccountException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserPrincipalService userPrincipalService;
    @Override
    public JWTToken login(LoginDto accountDto) throws AccountException {
        log.info("got here on log-in***-->{}",accountDto);
        return userPrincipalService.loginUser(accountDto);
    }
}
