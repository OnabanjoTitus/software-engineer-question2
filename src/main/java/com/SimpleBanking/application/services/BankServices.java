package com.SimpleBanking.application.services;

import com.SimpleBanking.application.data.model.AccountObject;
import com.SimpleBanking.application.web.Controller.ApiResponse;
import com.SimpleBanking.application.data.model.Account;
import com.SimpleBanking.application.data.model.AccountDto;
import com.SimpleBanking.application.data.model.AccountStatements;
import com.SimpleBanking.application.web.Exceptions.AccountException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BankServices {
    AccountObject getAccountInfo(String accountNumber, String accountPassword) throws AccountException;
    List<AccountStatements> getAccountStatement(String accountNumber,String accountPassword) throws AccountException;
    String  deposit(String accountNumber,Double amount) throws AccountException;
    String withdrawal(String accountNumber, String accountPassword,Double withdrawnAmount) throws AccountException;
    String createAccount(AccountDto accountDto) throws AccountException;
    Account findUserByAccountNumber(String accountNumber) throws AccountException;
    Account findUserByAccountName(String accountName) throws AccountException;


}
