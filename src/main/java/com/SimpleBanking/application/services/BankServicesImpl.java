package com.SimpleBanking.application.services;

import com.SimpleBanking.application.data.model.*;
import com.SimpleBanking.application.web.Exceptions.AccountException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;


@Slf4j
@Service
public class BankServicesImpl implements BankServices{
    @Autowired
    ModelMapper modelMapper;


    Map<String,Account>bankDatabase= new HashMap<>();


    @Override
    public AccountObject getAccountInfo(String accountNumber,String accountPassword) throws AccountException {
        Account account=bankDatabase.get(accountNumber);
        if(account==null){
            throw new AccountException("User With this account Number does not exist in our Database");
        }
        if(!account.getPassword().equals(accountPassword)){
            throw new AccountException("Dear customer your password is incorrect try again");
        }
        AccountObject account1= new AccountObject();
        account1.setAccountName(account.getAccountName());
        account1.setAccountNumber(account.getAccountNumber());
        account1.setAccountBalance(account.getAccountBalance());
        return account1;
    }

    @Override
    public List<AccountStatements> getAccountStatement(String accountNumber,String accountPassword) throws AccountException {
        Account account=bankDatabase.get(accountNumber);
        if(account==null){
            throw new AccountException("User With this account Number does not exist in our Database");
        }
        if(!account.getPassword().equals(accountPassword)){
            throw new AccountException("Dear customer your password is incorrect try again");
        }
        return account.getAccountStatementsList();
    }

    @Override
    public String deposit(String accountNumber, Double amount) throws AccountException {
        if(amount>=1000000.00||amount<1.0){
            throw new AccountException("Invalid deposit amount, please ensure the deposit is from #1.00 to #999,999");
        }
        Account account=bankDatabase.get(accountNumber);
        if(account==null){
            throw new AccountException("User With this account Number does not exist in our Database");
        }
        account.setAccountBalance(account.getAccountBalance()+amount);
        AccountStatements accountStatements= new AccountStatements();
        accountStatements.setTransactionDate(LocalDate.now());
        accountStatements.setTransactionType(TransactionType.DEPOSIT);
        accountStatements.setNarration("The sum of "+amount+" was deposited");
        accountStatements.setAmount(amount);
        accountStatements.setAccountBalance(account.getAccountBalance());
        if(account.getAccountStatementsList()==null){
            account.setAccountStatementsList(Collections.singletonList(accountStatements));
        }else {
          List<AccountStatements>accountStatements1=account.getAccountStatementsList();
          accountStatements1.add(accountStatements);
            account.setAccountStatementsList(accountStatements1);
        }
        bankDatabase.put(accountNumber,account);
        log.info("The bank account state is-->{}",bankDatabase.get(accountNumber));
        return transactionMessage(amount,account);
    }

    private String transactionMessage(Double amount,Account account) {
        String message="You have successfully deposited the sum of "+amount+" Into the account: "
                +"Account name: "+account.getAccountName()+" Account number: "+account.getAccountNumber();

        return message;

    }

    @Override
    public String withdrawal(String accountNumber, String accountPassword, Double withdrawnAmount) throws AccountException {
        if(withdrawnAmount<1.0){
            throw new AccountException("Please enter a valid amount");
        }
        Account account=bankDatabase.get(accountNumber);
        if(account==null){
            throw new AccountException("User With this account Number does not exist in our Database");
        }
        if(!account.getPassword().equals(accountPassword)){
            throw new AccountException("Dear customer your password is incorrect try again");
        }
        if((account.getAccountBalance()-withdrawnAmount)<500.0){
            throw new AccountException("You do not have insufficient funds to perform this transaction");
        }

        account.setAccountBalance(account.getAccountBalance()-withdrawnAmount);
        AccountStatements accountStatements= new AccountStatements();
        accountStatements.setTransactionDate(LocalDate.now());
        accountStatements.setTransactionType(TransactionType.WITHDRAWAL);
        accountStatements.setNarration("The sum of "+withdrawnAmount+" was withdrawn");
        accountStatements.setAmount(withdrawnAmount);
        accountStatements.setAccountBalance(account.getAccountBalance());
        if(account.getAccountStatementsList()==null){
            account.setAccountStatementsList(Collections.singletonList(accountStatements));
        }else {
            List<AccountStatements>accountStatements1=account.getAccountStatementsList();
            accountStatements1.add(accountStatements);
            account.setAccountStatementsList(accountStatements1);
        }
        bankDatabase.put(accountNumber,account);
        log.info("The bank account state is-->{}",bankDatabase.get(accountNumber));
        return transactionMessage2(withdrawnAmount,account);
    }

    private String transactionMessage2(Double withdrawnAmount, Account account) {
        String message="You have successfully withdrawn the sum of "+withdrawnAmount+" from the account: "
                +"Account name: "+account.getAccountName()+" Account number: "+account.getAccountNumber();
        return message;
    }

    @Override
    public String createAccount(AccountDto accountDto) throws AccountException {
        log.info("The Account Information received is -->{}",accountDto);
        if(accountDto.getAccountName().isBlank()){
            throw new AccountException("Account Name cannot be blank, please enter a valid name");
        }
        if(accountDto.getAccountPassword().isBlank()){
            throw new AccountException("Account Password cannot be blank, please enter a valid password");
        }
        log.info("The Account Information after mapping is received is -->{}",accountDto);
        Boolean isPresent=accountNameIsPresent(accountDto);
        if(isPresent){
            throw new AccountException("User With this account already exist in our Database");
        }
        if(accountDto.getInitialDeposit()<500.0){
            throw new AccountException("The Initial Deposit Must Be greater than or equal to #500.0");
        }
        Account account= new Account();
        account.setRole(Role.CUSTOMER);
        modelMapper.map(accountDto,account);
        account.setAccountBalance(accountDto.getInitialDeposit());
        //Todo create account number of 10 numbers
        UUID uuid = UUID.randomUUID();
        String accountNumberGenerated = Long.toString(uuid.getMostSignificantBits(), 96);
        accountNumberGenerated = accountNumberGenerated.substring(1,accountNumberGenerated.length()-8);
        log.info("The Account Lengths -->{}",accountNumberGenerated.length());
        account.setAccountNumber(accountNumberGenerated);

        log.info("The Account Information after mapping is received is -->{}",account);
        bankDatabase.put(account.getAccountNumber(),account);

        return accountCreationMessageTemplate(account);
    }

    private String accountCreationMessageTemplate(Account account) {

        String message = "Dear " + account.getAccountName() + ", your account has been successfully created with the following credentials:" +
                "  AccountName: " + account.getAccountName() +
                "  AccountNumber: " + account.getAccountNumber() +
                "  AccountBalance: " + account.getAccountBalance();
        return message;
    }

    private Boolean accountNameIsPresent(AccountDto account) {
        Account account2=bankDatabase.values()
                .stream().filter(account1 -> account.getAccountName().equals(account1.getAccountName())).findAny().orElse(null);
        log.info("The account found is -->{}",account2);
        if(account2!=null){
            return true;
        }else{
          return false;
        }
    }



    @Override
    public Account findUserByAccountNumber(String accountNumber) throws AccountException {
        Account account=bankDatabase.get(accountNumber);
        log.info("The acc found is-->{}",account);
        if(account!=null){
            return account;
        }
        throw new AccountException("User With this account Number does not exist in our Database");
    }

    @Override
    public Account findUserByAccountName(String accountName) throws AccountException {
        Account account2=bankDatabase.values()
                .stream().filter(account1 -> accountName.equals(account1.getAccountName())).findAny().orElse(null);
        log.info("The account found is -->{}",account2);
        return account2;
    }
}
