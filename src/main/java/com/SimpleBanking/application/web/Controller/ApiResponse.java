package com.SimpleBanking.application.web.Controller;

import com.SimpleBanking.application.data.model.AccountStatements;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
public class ApiResponse {
    private HttpStatus responseCode;
    private boolean success;
    private String message;


    public ApiResponse(HttpStatus responseCode, boolean success, String message) {
        this.responseCode = responseCode;
        this.success = success;
        this.message = message;
    }


}
