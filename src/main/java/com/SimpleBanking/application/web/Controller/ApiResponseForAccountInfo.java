package com.SimpleBanking.application.web.Controller;

import com.SimpleBanking.application.data.model.Account;
import com.SimpleBanking.application.data.model.AccountObject;
import lombok.Data;
import org.springframework.http.HttpStatus;
@Data
public class ApiResponseForAccountInfo {
    private HttpStatus responseCode;
    private boolean success;
    private String message;
    private AccountObject account;

    public ApiResponseForAccountInfo(HttpStatus responseCode, boolean success, String message, AccountObject account) {
        this.responseCode = responseCode;
        this.success = success;
        this.message = message;
        this.account = account;
    }
}
