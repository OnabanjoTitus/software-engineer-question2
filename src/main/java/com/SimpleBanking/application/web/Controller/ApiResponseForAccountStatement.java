package com.SimpleBanking.application.web.Controller;

import com.SimpleBanking.application.data.model.AccountStatements;
import lombok.Data;

import java.util.List;
@Data
public class ApiResponseForAccountStatement {
    private List<AccountStatements> accountStatements;
    public ApiResponseForAccountStatement(List<AccountStatements> accountStatements) {
        this.accountStatements = accountStatements;
    }
}
