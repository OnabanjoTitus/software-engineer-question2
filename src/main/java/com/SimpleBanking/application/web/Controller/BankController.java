package com.SimpleBanking.application.web.Controller;

import com.SimpleBanking.application.data.model.*;
import com.SimpleBanking.application.data.security.config.UserService;
import com.SimpleBanking.application.services.BankServices;
import com.SimpleBanking.application.web.Exceptions.AccountException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/simpleBank")
@Slf4j
public class BankController {

    @Autowired
    BankServices bankServices;
    @Autowired
    UserService userService;

    @PostMapping("/create_account")
    public ResponseEntity<?> registration(@RequestBody AccountDto accountDto){
        log.info("got here on log-in-->{}",accountDto);
        String accountInfo;
        try{
            accountInfo=bankServices.createAccount(accountDto);

        }
        catch ( AccountException exception){
            return new ResponseEntity<>(new ApiResponse(HttpStatus.BAD_REQUEST,false, exception.getMessage()),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new ApiResponse(HttpStatus.OK,true,accountInfo), HttpStatus.OK);

    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDto loginDto){
        log.info("got here on log-in*-->{}",loginDto);
        String token;

        try{
            bankServices.findUserByAccountNumber(loginDto.getAccountNumber());
            log.info("got here on log-in**-->{}",loginDto);
            token = userService.login(loginDto).getAccessToken();
        }
        catch ( AccountException exception){
            return new ResponseEntity<>(new ApiResponse(HttpStatus.UNAUTHORIZED,false, exception.getMessage()), HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(new ApiResponse(HttpStatus.OK,true, token), HttpStatus.OK);


    }
    @PostMapping("/deposit")
    public ResponseEntity<?> deposit(@RequestBody DepositDto depositDto){
        log.info("got here on log-in-->{}",depositDto);
        String accountInfo;
        try{
            accountInfo=bankServices.deposit(depositDto.getAccountNumber(),depositDto.getAmount());

        }
        catch ( AccountException exception){
            return new ResponseEntity<>(new ApiResponse(HttpStatus.BAD_REQUEST,false, exception.getMessage()),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new ApiResponse(HttpStatus.OK,true,accountInfo), HttpStatus.OK);

    }
    @PostMapping("/withdrawal")
    public ResponseEntity<?> withdrawal(@RequestBody WithdrawalDto withdrawalDto){
        log.info("got here on log-in-->{}",withdrawalDto);
        String accountInfo;
        try{
            accountInfo=bankServices.withdrawal(withdrawalDto.getAccountNumber(),withdrawalDto.getPassword(),withdrawalDto.getAmount());

        }
        catch ( AccountException exception){
            return new ResponseEntity<>(new ApiResponse(HttpStatus.BAD_REQUEST,false, exception.getMessage()),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new ApiResponse(HttpStatus.OK,true,accountInfo), HttpStatus.OK);

    }
    @GetMapping("/account_statement/{accountNumber}")
    public ResponseEntity<?> accountStatement(@RequestBody AccountDto accountDto, @PathVariable String accountNumber){
        log.info("got here on log-in-->{}",accountDto);
      List<AccountStatements> accountInfo;
        try{
            accountInfo=bankServices.getAccountStatement(accountNumber,accountDto.getAccountPassword());

        }
        catch ( AccountException exception){
            return new ResponseEntity<>(new ApiResponse(HttpStatus.BAD_REQUEST,false, exception.getMessage()),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new ApiResponseForAccountStatement(accountInfo), HttpStatus.OK);

    }
    @GetMapping("/account_info/{accountNumber}")
    public ResponseEntity<?> accountInfo(@RequestBody AccountDto accountDto, @PathVariable String accountNumber){
        log.info("got here on log-in-->{}",accountDto);
        AccountObject account;
        try{
            account=bankServices.getAccountInfo(accountNumber,accountDto.getAccountPassword());

        }
        catch ( AccountException exception){
            return new ResponseEntity<>(new ApiResponse(HttpStatus.BAD_REQUEST,false, exception.getMessage()),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new ApiResponseForAccountInfo(HttpStatus.OK,true,"successful",account), HttpStatus.OK);

    }


}
