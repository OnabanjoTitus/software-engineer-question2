package com.SimpleBanking.application.Question1;

public class TotalNumberOfSquareException extends Exception {
    public TotalNumberOfSquareException() {
    }

    public TotalNumberOfSquareException(String message) {
        super(message);
    }

    public TotalNumberOfSquareException(String message, Throwable cause) {
        super(message, cause);
    }

    public TotalNumberOfSquareException(Throwable cause) {
        super(cause);
    }
}
