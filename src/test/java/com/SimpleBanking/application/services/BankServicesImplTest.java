package com.SimpleBanking.application.services;

import com.SimpleBanking.application.data.model.Account;
import com.SimpleBanking.application.data.model.AccountDto;
import com.SimpleBanking.application.web.Exceptions.AccountException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class BankServicesImplTest {

    @Autowired
    BankServices bankServices;

    AccountDto accountDto;
    @BeforeEach
    void setUp(){

        accountDto= new AccountDto();
    }

    @AfterEach
    void tearDown() {
        accountDto=null;

    }

    @Test
    void getAccountInfo() throws AccountException {
        accountDto.setAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountPassword("12345");
        accountDto.setInitialDeposit(10000.0);
        bankServices.createAccount(accountDto);
        Account account=bankServices.findUserByAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountNumber(account.getAccountNumber());
        bankServices.getAccountInfo(accountDto.getAccountNumber(),accountDto.getAccountPassword());
    }

    @Test
    void getAccountStatement() throws AccountException {
        accountDto.setAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountPassword("12345");
        accountDto.setInitialDeposit(10000.0);
        bankServices.createAccount(accountDto);
        Account account=bankServices.findUserByAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountNumber(account.getAccountNumber());
        bankServices.getAccountStatement(accountDto.getAccountNumber(),accountDto.getAccountPassword());
    }

    @Test
    void deposit() throws AccountException {
        accountDto.setAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountPassword("12345");
        accountDto.setInitialDeposit(10000.0);
        bankServices.createAccount(accountDto);
        Account account=bankServices.findUserByAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountNumber(account.getAccountNumber());
        bankServices.deposit(accountDto.getAccountNumber(),100.0);
        assertEquals(10100.0,account.getAccountBalance());
    }

    @Test
    void withdrawal() throws AccountException {
        accountDto.setAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountPassword("12345");
        accountDto.setInitialDeposit(600.0);
        bankServices.createAccount(accountDto);
        Account account=bankServices.findUserByAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountNumber(account.getAccountNumber());
        bankServices.withdrawal(accountDto.getAccountNumber(),"12345",100.0);
        assertEquals(500.0,account.getAccountBalance());
    }

    @Test
    void createAccount() throws AccountException {
        accountDto.setAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountPassword("12345");
        accountDto.setInitialDeposit(600.0);
        bankServices.createAccount(accountDto);
        Account account=bankServices.findUserByAccountName("Onabanjo Titus Damilola");
        assertTrue(account.isEnabled());
    }

    @Test
    void findUserByAccountNumber() throws AccountException {
        accountDto.setAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountPassword("12345");
        accountDto.setInitialDeposit(600.0);
        bankServices.createAccount(accountDto);
        Account account=bankServices.findUserByAccountName("Onabanjo Titus Damilola");
        accountDto.setAccountNumber(account.getAccountNumber());
        Account account1=bankServices.findUserByAccountNumber(account.getAccountNumber());
        assertEquals(account,account1);
    }
}